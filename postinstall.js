#!/usr/bin/env node
import fs from "fs"
import path from "path"

const destFile = path.resolve("../../../prettier.config.js")

if (!fs.existsSync(destFile)) {
	fs.copyFile(path.resolve("./config.js"), destFile, err => {
		if (err) console.error(err)
	})
}
