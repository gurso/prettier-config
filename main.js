export default {
	useTabs: true,
	semi: false,
	arrowParens: "avoid",
	printWidth: 120,
}
